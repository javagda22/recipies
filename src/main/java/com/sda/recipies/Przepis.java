package com.sda.recipies;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Przepis {
    private String nazwa;
    private PoraRoku poraRoku;
    private String trescPrzepisu;
    private int czasWykonania;
    private List<KrokPrzepisu> listaKrokow;
    private List<Skladnik> listaSkladnikow;

}
