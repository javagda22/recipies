package com.sda.recipies;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Skladnik {
    private String nazwa;
    private double ilosc;
}
