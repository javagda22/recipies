package com.sda.recipies;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Klasa Składnik ma pola:
 * - nazwa
 * - ilość (double)
 * <p>
 * Klasa KrokPrzepisu ma pola:
 * - treść kroku
 * <p>
 * Enum PoraRoku ma wartości:
 * - WIOSNA, LATO, JESIEN, ZIMA
 * <p>
 * Klasa Przepis ma pola:
 * - nazwa przepisu
 * - poraRoku (typu PoraRoku)
 * - treść przepisu
 * - czas wykonywania dania (int w minutach)
 * - Listę kroków (List<KrokPrzepisu>)
 * - Listę składników (List<Skladnik>)
 * <p>
 * <p>
 * 1. wybierz przepisy które mają mniej niż dwa składniki
 * 2. wybierz przepisy które mają wiecej niż 10 kroków.
 * 3. znajdź przepisy na porę roku LATO
 * 4. znajdź przepisy dłuższe (ilość kroków) niż 5 i które na liście składników zawierają "Jajko" w ilości 5
 * 5. zbierz kolekcję wszystkich unikalnych składników. Nie bierz pod uwagę ilości składnika, tylko jego nazwę podczas porównania
 * 6. Zbierz kolekcję wszystkich unikalnych kroków
 * 7. Zbierz kolekcję wszystkich treści przepisów (string'ów)
 * 8. Zbierz kolekcję wszystkich unikalnych nazw składników.
 * 9. Zbierz kolekcję składników z przepisów na zimę, z których przepisy mają mniej więcej 5 kroków.
 */
public class Main {
    private static List<Skladnik> przygotujListeSkladnikow(List<Skladnik> skladniks) {
        List<Skladnik> skladniksy = skladniks
                .stream()
                .collect(Collectors.groupingBy(Skladnik::getNazwa, Collectors.summingDouble(Skladnik::getIlosc)))
                .entrySet()
                .stream()
                .map(stringDoubleEntry -> new Skladnik(stringDoubleEntry.getKey(), stringDoubleEntry.getValue()))
                .collect(Collectors.toList());

        return skladniksy;
    }

    public static void main(String[] args) {
        List<Przepis> przepis = new ArrayList<>();

        // różne
        Skladnik s_jajko = new Skladnik("jajko", 1);
        Skladnik s_szkl_maka = new Skladnik("maka", 330); //ml
        Skladnik s_szkl_mleko = new Skladnik("mleko", 330); //ml
        Skladnik s_aromat_wanilia = new Skladnik("wanilia", 5); //ml
        Skladnik s_masło = new Skladnik("masło", 100); //g
        Skladnik s_barwnik = new Skladnik("barwnik", 1); //szt
        Skladnik s_herbata = new Skladnik("herbata", 1); //szt

        // mięsa
        Skladnik s_mieso_mielone = new Skladnik("mięso mielone", 100); //kg
        Skladnik s_mieso_kurczak_piers = new Skladnik("mięso kurczak pierś", 100); //kg
        Skladnik s_mieso_wolowe = new Skladnik("mięso wołowe", 100); //kg
        Skladnik s_mieso_wieprzowe = new Skladnik("mięso wieprzowe", 100); //kg

        // ryby
        Skladnik s_losos = new Skladnik("łosoś", 100); //g
        Skladnik s_dorsz = new Skladnik("dorsz", 100); //g
        Skladnik s_sledz = new Skladnik("śledź", 100); //g

        // przyprawy
        Skladnik s_bazylia = new Skladnik("bazylia", 1); //szczypta
        Skladnik s_oregano = new Skladnik("oregano", 1); //szczypta
        Skladnik s_curry = new Skladnik("curry", 1); //szczypta
        Skladnik s_papryka = new Skladnik("papryka", 1); //szczypta
        Skladnik s_pieprz = new Skladnik("pieprz", 1); //szczypta
        Skladnik s_sól = new Skladnik("sól", 1); //szczypta

        // warzywa
        Skladnik s_marchewka = new Skladnik("marchewka", 100); //g
        Skladnik s_szczypiorek = new Skladnik("szczypiorek", 100); //g
        Skladnik s_ziemniaki = new Skladnik("ziemniaki", 100); //g
        Skladnik s_bataty = new Skladnik("bataty", 100); //g
        Skladnik s_sałata = new Skladnik("sałata", 100); //g
        Skladnik s_papryka_cala = new Skladnik("papryka cała", 100); //g
        Skladnik s_oliwki = new Skladnik("oliwki", 100); // g
        Skladnik s_pomidor = new Skladnik("pomidor", 100); // g
        Skladnik s_ogórek = new Skladnik("ogórek", 100); // g
        Skladnik s_brokuła = new Skladnik("brokuła", 100); // g
        Skladnik s_fasola = new Skladnik("fasola", 100); // g
        Skladnik s_cebula = new Skladnik("cebula", 100); // g
        Skladnik s_avocado = new Skladnik("avocado", 100); // g
        Skladnik s_dynia = new Skladnik("dynia", 100); // g
        Skladnik s_fasola_czerowna = new Skladnik("fasola czerwona", 100); // g
        Skladnik s_pietruszka = new Skladnik("pietruszka", 100); // g
        Skladnik s_brukiew = new Skladnik("brukiew", 100); // g
        Skladnik s_burak = new Skladnik("burak", 100); // g
        Skladnik s_rzodkiewka = new Skladnik("rzodkiewka", 100); // g
        Skladnik s_seler = new Skladnik("seler", 100); // g
        Skladnik s_por = new Skladnik("por", 100); // g
        Skladnik s_czosnek = new Skladnik("czosnek", 100); // g
        Skladnik s_sok = new Skladnik("sok", 100); // ml
        Skladnik s_żelatyna = new Skladnik("żelatyna", 10); // g

        // sery
        Skladnik s_ser_edamski = new Skladnik("ser edamski", 100); // g
        Skladnik s_ser_gouda = new Skladnik("ser gouda", 100); //g
        Skladnik s_ser_mozarella = new Skladnik("ser mozarella", 100); //g
        Skladnik s_ser_parmezan = new Skladnik("ser parmezan", 100); //g
        Skladnik s_ser_cheddar = new Skladnik("ser cheddar", 100); //g

        // owoce
        Skladnik s_jabłko = new Skladnik("jabłko", 100); //g
        Skladnik s_brzoskwinia = new Skladnik("brzoskwinia", 100); //g
        Skladnik s_nektarynka = new Skladnik("nektarynka", 100); //g
        Skladnik s_gruszka = new Skladnik("gruszka", 100); //g
        Skladnik s_winogrona = new Skladnik("winogrona", 100); //g
        Skladnik s_borówka = new Skladnik("borówka", 100); //g
        Skladnik s_malina = new Skladnik("malina", 100); //g
        Skladnik s_pomarancza = new Skladnik("pomarancza", 100); //g
        Skladnik s_truskawka = new Skladnik("truskawka", 100); //g
        Skladnik s_banan = new Skladnik("banan", 100); //g
        Skladnik s_papaya = new Skladnik("papaya", 100); //g
        Skladnik s_cytryna = new Skladnik("cytryna", 100); //g
        Skladnik s_kiwi = new Skladnik("kiwi", 100); //g
        Skladnik s_arbuz = new Skladnik("arbuz", 100); //g
        Skladnik s_cukier = new Skladnik("cukier", 10); //g
        Skladnik s_woda = new Skladnik("woda", 100); //ml
        Skladnik s_miód = new Skladnik("miód", 100); //ml

        // inne
        Skladnik s_oliwa = new Skladnik("oliwa", 100); // g
        Skladnik s_kminek = new Skladnik("kminek", 1); // szczypta
        Skladnik s_imbir = new Skladnik("imbir", 10); // g
        Skladnik s_tymianek = new Skladnik("tymianek", 1); // szczypta

        KrokPrzepisu k_umyj_owoce = new KrokPrzepisu("umyj owoce");
        KrokPrzepisu k_wbij_jajka = new KrokPrzepisu("wbij jajka");
        KrokPrzepisu k_umyj_warzywa = new KrokPrzepisu("umyj warzywa");
        KrokPrzepisu k_pokrój_owoce = new KrokPrzepisu("pokrój owoce");
        KrokPrzepisu k_pokrój_warzywa = new KrokPrzepisu("pokrój warzywa");
        KrokPrzepisu k_wrzuc_ziemniaki = new KrokPrzepisu("wrzuć ziemniaki");
        KrokPrzepisu k_wrzuc_owoce = new KrokPrzepisu("wrzuć owoce");
        KrokPrzepisu k_dopraw_do_smaku = new KrokPrzepisu("dopraw do smaku");
        KrokPrzepisu k_wrzuc_warzywa = new KrokPrzepisu("wrzuć warzuwa");
        KrokPrzepisu k_wrzuc_cebulke = new KrokPrzepisu("wrzuć cebulkę");
        KrokPrzepisu k_przysmażaj = new KrokPrzepisu("przysmażaj");
        KrokPrzepisu k_dopraw = new KrokPrzepisu("dopraw");
        KrokPrzepisu k_dosp_przypraw = new KrokPrzepisu("dosyp przypraw");
        KrokPrzepisu k_rozgrzej_patelnie = new KrokPrzepisu("rozgrzej patelnie");
        KrokPrzepisu k_zagotuj_wode = new KrokPrzepisu("zagotuj wode");
        KrokPrzepisu k_wrzuc_czosnek = new KrokPrzepisu("wrzuc czosnek");
        KrokPrzepisu k_wrzuc_bulion = new KrokPrzepisu("wrzuc bulion");
        KrokPrzepisu k_podaj_do_stolu = new KrokPrzepisu("podaj do stołu");
        KrokPrzepisu k_dolewaj_powoli = new KrokPrzepisu("dolewaj powoli");
        KrokPrzepisu k_mieszaj = new KrokPrzepisu("mieszaj");
        KrokPrzepisu k_obierz_dynie = new KrokPrzepisu("obierz dynie");
        KrokPrzepisu k_wrzuc_dynie = new KrokPrzepisu("wrzuć dynie");
        KrokPrzepisu k_zalej_wodą = new KrokPrzepisu("zalej wodą");
        KrokPrzepisu k_zagotuj = new KrokPrzepisu("zagotuj");
        KrokPrzepisu k_zmiksuj = new KrokPrzepisu("zmiksuj");
        KrokPrzepisu k_przemieszaj = new KrokPrzepisu("przemieszaj");
        KrokPrzepisu k_rozpusc_w_wodzie = new KrokPrzepisu("rozpusc w wodzie");
        KrokPrzepisu k_podgrzewaj = new KrokPrzepisu("podgrzewaj");
        KrokPrzepisu k_wstaw_do_lodówki = new KrokPrzepisu("wstaw do lodówki");
        KrokPrzepisu k_dosyp_make = new KrokPrzepisu("dosyp mąkę");
        KrokPrzepisu k_dosyp_cukru = new KrokPrzepisu("dosyp cukru");
        KrokPrzepisu k_wlej_mase = new KrokPrzepisu("wlej mase");
        KrokPrzepisu k_zdejmij = new KrokPrzepisu("zdejmij z patelni");
        KrokPrzepisu k_powtarzaj = new KrokPrzepisu("powtarzaj");
        KrokPrzepisu k_umyj_mieso = new KrokPrzepisu("umyj mieso");
        KrokPrzepisu k_pokrój_mieso = new KrokPrzepisu("pokrój mieso");
        KrokPrzepisu k_wrzuć_mieso = new KrokPrzepisu("wrzuć mieso");
        KrokPrzepisu k_pokrój = new KrokPrzepisu("pokrój");
        KrokPrzepisu k_dodaj_reszte = new KrokPrzepisu("dosyp reszte");
        KrokPrzepisu k_piecz = new KrokPrzepisu("piecz");


        // #######################################################################
        // #######################################################################
        // #######################################################################
        // #######################################################################
        // Przepis 1 - zupa z dyni:
        Przepis przepis_zupa_z_dyni = new Przepis();
        przepis_zupa_z_dyni.setCzasWykonania(120);
        przepis_zupa_z_dyni.setNazwa("Zupa z dyni");
        przepis_zupa_z_dyni.setPoraRoku(PoraRoku.JESIEN);
        przepis_zupa_z_dyni.setTrescPrzepisu("1. Warzywa myjemy, osuszamy i obieramy. Marchewkę, pietruszkę, seler i ziemniaki pokroimy w kostkę.\n" +
                "Z papryczki usuwamy nasiona. Cebulę, czosnek i papryczkę drobno siekamy.\n" +
                "Oczyszczoną dynię kroimy na drobne kawałki.\n" +
                "2. W rondlu rozgrzewamy oliwę i masło, wrzucamy cebulę z czosnkiem, szklimy (2 minuty), dodajemy posiekaną papryczkę i pozostałe warzywa (marchew, pietruszkę, seler i ziemniaki), oprószamy słodką papryką.\n" +
                "Obsmażamy aż warzywa nabiorą złotego koloru (ok. 7 minut).\n" +
                "Pokrojoną dynię dodajemy do przesmażonych warzyw i smażymy jeszcze ok. 2 minuty. Doprawiamy pieprzem, imbirem, tymiankiem, liściem laurowym, zielem angielskim, kminkiem i odrobiną soli, dokładnie mieszamy.\n" +
                "3. Zalewamy ok. 1-1,2 litra gorącej wody i szklanką bulionu, mieszamy, gotujemy pod przykryciem, od czasu do czasu mieszając przez ok. 20-25 minut, aż warzywa będą miękkie.\n" +
                "Wyjmujemy liść laurowy i ziele angielskie, całość miksujemy blenderem na gładką masę.\n" +
                "\n" +
                "Podajemy z grzankami, np. z grzanką serowo-tymiankową ze swojskiego chleba i kleksem jogurtu naturalnego.");
        przepis_zupa_z_dyni.setListaSkladnikow(przygotujListeSkladnikow(Arrays.asList(
                s_dynia, s_dynia, s_dynia, s_dynia, s_dynia, s_dynia, s_dynia, s_dynia, s_dynia, s_dynia, // 1 kg dyni
                s_cebula, // 100 g cebuli
                s_pietruszka,
                s_marchewka, s_marchewka,
                s_papryka_cala,
                s_seler,
                s_ziemniaki, s_ziemniaki, s_ziemniaki,
                s_oliwa,
                s_czosnek,
                s_kminek,
                s_imbir,
                s_tymianek,
                s_sól,
                s_pieprz
        )));
        przepis_zupa_z_dyni.setListaKrokow(new ArrayList<>(Arrays.asList(
                k_umyj_warzywa,
                k_obierz_dynie, k_pokrój_warzywa, k_wrzuc_cebulke, k_przysmażaj,
                k_wrzuc_ziemniaki, k_wrzuc_dynie,
                k_dopraw,
                k_zagotuj_wode,
                k_wrzuc_bulion,
                k_zalej_wodą,
                k_mieszaj,
                k_zagotuj,
                k_dopraw_do_smaku,
                k_zmiksuj,
                k_podaj_do_stolu
        )));

        // #######################################################################
        // #######################################################################
        // #######################################################################
        // #######################################################################
        Przepis przepis_galaretka_jablkowa = new Przepis();
        przepis_galaretka_jablkowa.setNazwa("Galaretka jabłkowa");
        przepis_galaretka_jablkowa.setPoraRoku(PoraRoku.LATO);
        przepis_galaretka_jablkowa.setCzasWykonania(20);
        przepis_galaretka_jablkowa.setListaSkladnikow(przygotujListeSkladnikow(Arrays.asList(
                s_woda, s_woda, s_woda, s_woda, s_woda, s_żelatyna,
                s_cukier, s_cukier,
                s_jabłko, s_jabłko, s_jabłko, s_jabłko,
                s_sok)));
        przepis_galaretka_jablkowa.setListaKrokow(new ArrayList<>(Arrays.asList(
                k_zagotuj_wode,
                k_rozpusc_w_wodzie,
                k_podgrzewaj,
                k_dolewaj_powoli, // sok
                k_mieszaj,
                k_wrzuc_owoce,
                k_wstaw_do_lodówki
        )));
        przepis_galaretka_jablkowa.setTrescPrzepisu("W rondelku rozpuścić żelatynę w wodzie. Dokładnie wymieszać i poczekać aż zmięknie. Zagotować na małym ogniu i odstawić z palnika. Mieszając powoli należy dolać sok owocowy. Całość dokładnie wymieszać. Mieszankę należy wlać do małych miseczek. Dodać owoce i wymieszać. Wstawić do lodówki, aż galaretka stężeje.");

        // #######################################################################
        // #######################################################################
        // #######################################################################
        // #######################################################################
        Przepis przepis_nalesniki = new Przepis();
        przepis_nalesniki.setNazwa("Naleśniki");
        przepis_nalesniki.setCzasWykonania(30);
        przepis_nalesniki.setTrescPrzepisu("Mąkę wsypać do miski, dodać jajka, mleko, wodę i sól. Zmiksować na gładkie ciasto. Dodać roztopione masło lub olej roślinny i razem zmiksować (lub wykorzystać tłuszcz do smarowania patelni przed smażeniem każdego naleśnika).\n" +
                "Naleśniki smażyć na dobrze rozgrzanej patelni z cienkim dnem np. naleśnikowej. Przewrócić na drugą stronę gdy spód naleśnika będzie już ładnie zrumieniony i ścięty.");
        przepis_nalesniki.setListaSkladnikow(przygotujListeSkladnikow(Arrays.asList(s_jajko, s_jajko, s_szkl_mleko, s_szkl_maka, s_woda, s_woda, s_sól, s_oliwa)));
        przepis_nalesniki.setListaKrokow(new ArrayList<>(Arrays.asList(
                k_wbij_jajka,
                k_przemieszaj,
                k_dosyp_make,
                k_przemieszaj,
                k_dosyp_cukru,
                k_przemieszaj,
                k_rozgrzej_patelnie,
                k_wlej_mase,
                k_przysmażaj,
                k_zdejmij,
                k_wlej_mase,
                k_powtarzaj,
                k_podaj_do_stolu
        )));

        Przepis przepis_pancake = new Przepis();
        przepis_pancake.setNazwa("Pancake");
        przepis_nalesniki.setCzasWykonania(30);
        przepis_nalesniki.setTrescPrzepisu("W misce połącz sypkie składniki - przesiej mąkę, sól, proszek do pieczenia i cukier. Wymieszaj. Roztrzep jajka, połącz je z roztopionym masłem oraz mlekiem. Teraz połącz suche składniki z masą jajeczną. Wymieszaj lub zmiksuj, aż masy się połączą.\n" +
                "\n Pancakes smaż na suchej patelni, aż będą złote z obu stron. Uważaj przy przekręcaniu! Naleśniki amerykańskie serwuj z ulubionymi dodatkami: masłem orzechowym, świeżymi owocami, syropem klonowym albo bitą śmietaną.");

        // #######################################################################
        // #######################################################################
        // #######################################################################
        // #######################################################################
        Przepis przepis_skrzydelka_w_miodzie = new Przepis();
        przepis_skrzydelka_w_miodzie.setNazwa("Skrzydełka w miodzie");
        przepis_skrzydelka_w_miodzie.setCzasWykonania(100);
        przepis_skrzydelka_w_miodzie.setPoraRoku(PoraRoku.ZIMA);
        przepis_skrzydelka_w_miodzie.setListaSkladnikow(przygotujListeSkladnikow(Arrays.asList(
                s_mieso_kurczak_piers,
                s_mieso_kurczak_piers,
                s_mieso_kurczak_piers,
                s_mieso_kurczak_piers,
                s_mieso_kurczak_piers,
                s_marchewka,
                s_miód,
                s_miód,
                s_curry,
                s_sól,
                s_pieprz,
                s_papryka,
                s_papryka_cala,
                s_czosnek,
                s_oliwa
        )));
        przepis_skrzydelka_w_miodzie.setListaKrokow(new ArrayList<>(Arrays.asList(
                k_rozgrzej_patelnie,
                k_umyj_warzywa,
                k_umyj_mieso,
                k_pokrój_mieso,
                k_wrzuc_warzywa,
                k_wrzuć_mieso,
                k_przysmażaj,
                k_dolewaj_powoli, //miodu
                k_podgrzewaj,
                k_przysmażaj
        )));
        przepis_skrzydelka_w_miodzie.setTrescPrzepisu("Skrzydełka myjemy i osuszamy. Miód, musztardę, oliwę, sól, pieprz, paprykę i rozgnieciony czosnek mieszamy ze sobą do uzyskania jednolitej konsystencji. Do tak przygotowanej marynaty wkładamy skrzydełka i nacieramy je dokładnie. Wkładamy do lodówki na min. 1 godzinę.\n" +
                "Przekładamy do żaroodpornego naczynia. Pieczemy ok. 40 min w piekarniku rozgrzanym do temp. 170 stopni. Co 10 min polewamy wytopionym tłuszczem zmieszanym z resztką marynaty. Pod sam koniec zwiększamy temperaturę do 200 stopni i pieczemy jeszcze 10 minut.\n" +
                "Podajemy z ziemniaczkami lub ryżem.\n");

        // #######################################################################
        // #######################################################################
        // #######################################################################
        // #######################################################################
        Przepis przepis_ciastka = new Przepis();
        przepis_ciastka.setPoraRoku(PoraRoku.ZIMA);
        przepis_ciastka.setTrescPrzepisu("Using a hand mixer or standing mixer, beat butter until creamy.\n" +
                "Add powdered sugar and beat until combined. Add the eggs, vanilla, and salt. Beat until well combined.\n" +
                "Gradually, add the flour until dough is moist and holds together, but doesn’t stick to your hands.\n" +
                "Place dough on a cutting board and form into a ball. Cut in half, then cover one half with plastic wrap and set aside.\n" +
                "Divide the other half into 6 pieces. The outer layers of the rainbow will need a little bit more dough, so make two of them slightly larger than the others.\n" +
                "Dye the dough with food coloring by placing a ¼ teaspoon of dye on the ball of dough and kneading it until it is one solid color. Gloves optional but suggested.\n" +
                "Starting with purple, roll the dough into a log about 6-8 inches (15-20 cm) long. This will be the center of the rainbow.\n" +
                "Next, roll out the blue dough with a rolling pin until it’s about ¼ inch (6 mm) thick, slightly longer than the purple, and wide enough to wrap around the purple dough. Repeat with the green, yellow, orange, and red doughs. The further away from the purple you get, the wider the dough needs to be to go around the previous color, making the red dough the widest.\n" +
                "Wrap the blue dough around the purple, pinching together any cracks that form, cutting off any excess dough. Repeat with the rest of the colors.\n" +
                "Wrap in plastic wrap and chill for 30 minutes.\n" +
                "Once chilled, remove the plastic wrap, and cut the dough in half to make 2 rainbow shapes. Set aside.\n" +
                "Cut the reserved dough in half, roll out to ¼-inch (6 mm) thickness, and make it slightly longer than the rainbow log.\n" +
                "Place the rainbow dough in the center and wrap the plain dough around it, again, pinching together any cracks and smoothing out the surface.\n" +
                "Repeat with the other halves. Wrap with plastic wrap and chill for another 30 minutes.\n" +
                "Preheat oven to 350˚F (180˚C).\n" +
                "Once chilled, remove the plastic wrap. Slice cookies about ½-inch (1 cm) thick and place on a parchment paper-lined baking sheet.\n" +
                "Bake for 20 minutes or until the bottom has browned slightly.\n" +
                "Cool on a cooling rack.");
        przepis_ciastka.setNazwa("Ciastka tęczowe");
        przepis_ciastka.setListaSkladnikow(przygotujListeSkladnikow(Arrays.asList(
                s_masło, s_masło,
                s_cukier, s_cukier, s_cukier, s_cukier, s_cukier, s_cukier, s_cukier, s_cukier, s_cukier, s_cukier,
                s_aromat_wanilia,
                s_sól,
                s_jajko, s_jajko, s_jajko,
                s_szkl_maka,
                s_szkl_maka,
                s_barwnik, s_barwnik, s_barwnik, s_barwnik // 4 kolory
        )));
        przepis_ciastka.setListaKrokow(Arrays.asList(
                k_zmiksuj,
                k_dosyp_cukru,
                k_dopraw,
                k_dosyp_make,
                k_pokrój,
                k_zmiksuj,
                k_przemieszaj,
                k_podgrzewaj,
                k_wstaw_do_lodówki,
                k_pokrój,
                k_podgrzewaj,
                k_podaj_do_stolu
        ));

        Przepis przepis_sernik = new Przepis();
        przepis_sernik.setNazwa("Sernik");
        przepis_sernik.setPoraRoku(PoraRoku.JESIEN);
        przepis_sernik.setCzasWykonania(180);
        przepis_sernik.setTrescPrzepisu("Preheat oven to 350ºF (180ºC).\n" +
                "In a bowl, add the crushed graham crackers and melted butter and stir to combine.\n" +
                "Place the graham cracker mixture in a greased 8-inch (20-cm) springform pan, spreading them in an even layer. Bake for 20 minutes or until in turns slightly golden on the top.\n" +
                "In a bowl, mix the cream cheese and sugar using a hand mixer until smooth.\n" +
                "Add the eggs and vanilla, and continue beating until all ingredients are fully incorporated.\n" +
                "Add the cream cheese mixture to the top of the graham cracker crust.\n" +
                "Bake for 1 hour, or until it looks semi-jiggly in the center and set on the edges.\n" +
                "Refrigerate for 12 hours, or overnight.");
        przepis_sernik.setListaSkladnikow(przygotujListeSkladnikow(Arrays.asList(
                s_szkl_maka, s_szkl_maka, s_szkl_maka, s_szkl_maka, s_szkl_maka,
                s_masło, s_masło, s_cukier, s_cukier, s_cukier,
                s_jajko, s_jajko, s_jajko, s_jajko, s_aromat_wanilia,
                s_borówka, s_borówka
        )));
        przepis_sernik.setListaKrokow(Arrays.asList(
                k_wbij_jajka,
                k_przemieszaj,
                k_zmiksuj,
                k_dodaj_reszte,
                k_dosyp_make,
                k_piecz,
                k_wstaw_do_lodówki,
                k_podaj_do_stolu
        ));

        Przepis przepis_klopsiki = new Przepis();
        przepis_klopsiki.setTrescPrzepisu("Preheat the oven to 375˚F (190˚C).\n" +
                "Make the sauce: In a medium bowl, stir together the tomato sauce, apple cider vinegar, and honey. Set aside.\n" +
                "Make the meatloaf: In a large bowl, combine the ground turkey, zucchini, onion, garlic, egg, Dijon mustard, honey, 2 tablespoons of the tomato sauce, bread crumbs, salt, pepper, oregano, basil, and parsley. Mix well.\n" +
                "Add half of the meatloaf mixture to a greased loaf pan and flatten to form an even layer.\n" +
                "Layer the cheese over the meatloaf layer. Top with the rest of the meatloaf and smooth the top.\n" +
                "Top the meatloaf with half of the tomato sauce mixture, using a spoon or rubber spatula to spread evenly. Reserve the rest of the tomato sauce.\n" +
                "Bake the meatloaf for 1 hour, or until the internal temperature reads 160˚F (70˚C) when tested with a meat thermometer.\n" +
                "When the meatloaf is cooked through, warm the reserved tomato sauce in the microwave or a skillet, then drizzle on top of the meatloaf.\n" +
                "Garnish with fresh parsley and serve with your favorite sides. (We used mashed sweet potatoes and asparagus.)\n" +
                "Enjoy!");
        przepis_klopsiki.setNazwa("Meatloaf");
        przepis_klopsiki.setCzasWykonania(120);
        przepis_klopsiki.setPoraRoku(PoraRoku.WIOSNA);
        przepis_klopsiki.setListaSkladnikow(przygotujListeSkladnikow(Arrays.asList(
                s_mieso_mielone,
                s_mieso_mielone,
                s_mieso_mielone,
                s_mieso_mielone,
                s_pomidor,
                s_pomidor,
                s_pomidor,
                s_miód,
                s_cebula,
                s_cebula,
                s_jajko,
                s_sól,
                s_pieprz,
                s_oregano,
                s_bazylia,
                s_ser_mozarella,
                s_ser_mozarella,
                s_czosnek
        )));
        przepis_klopsiki.setListaKrokow(Arrays.asList(
                k_rozgrzej_patelnie,
                k_pokrój_warzywa,
                k_wrzuc_warzywa,
                k_wrzuc_czosnek,
                k_dopraw,
                k_wbij_jajka,
                k_przemieszaj,
                k_wrzuć_mieso,
                k_przemieszaj,
                k_dodaj_reszte,
                k_przemieszaj,
                k_przysmażaj,
                k_zdejmij,
                k_przemieszaj,
                k_wrzuć_mieso,
                k_przysmażaj,
                k_powtarzaj,
                k_zdejmij
        ));
        Przepis przepis_herbata = new Przepis();
        przepis_herbata.setNazwa("Herbata");
        przepis_herbata.setCzasWykonania(5);
        przepis_herbata.setPoraRoku(PoraRoku.ZIMA);
        przepis_herbata.setTrescPrzepisu("Herbatę wsypać do zaparzacza i zalać wrzącą wodą do 2/3 szklanki. Parzyć pod przykryciem ok. 5 minut. Następnie herbatę wysypać i w zaparzaczu umieścić startą skórkę pomarańczy. Herbatę z pomarańczą parzyć ok. 3 minut. W międzyczasie sok pomarańczowy i cukier podgrzać w rondelku, aż cukier się rozpuści. Przelać do herbaty i wymieszać.");
        przepis_herbata.setListaSkladnikow(przygotujListeSkladnikow(Arrays.asList(
                s_cytryna,
                s_herbata,
                s_woda
        )));
        przepis_herbata.setListaKrokow(Arrays.asList(
                k_zagotuj_wode,
                k_dodaj_reszte,
                k_podaj_do_stolu
        ));

        Przepis przepis_kawa = new Przepis();
        przepis_kawa.setNazwa("Kawa");
        przepis_kawa.setCzasWykonania(5);
        przepis_kawa.setPoraRoku(PoraRoku.LATO);
        przepis_kawa.setTrescPrzepisu("Wsyp kawe, zalej wodą, dosyp cukru do smaku i tadaaaa! kawa! :)");
        przepis_kawa.setListaSkladnikow(przygotujListeSkladnikow(Arrays.asList(
                s_cytryna,
                s_herbata,
                s_woda
        )));
        przepis_kawa.setListaKrokow(Arrays.asList(
                k_zagotuj_wode,
                k_dodaj_reszte,
                k_podaj_do_stolu
        ));

        List<Przepis> przepisy = new ArrayList<>();
        przepisy.add(przepis_galaretka_jablkowa);
        przepisy.add(przepis_nalesniki);
        przepisy.add(przepis_pancake);
        przepisy.add(przepis_zupa_z_dyni);
        przepisy.add(przepis_skrzydelka_w_miodzie);
        przepisy.add(przepis_ciastka);
        przepisy.add(przepis_sernik);
        przepisy.add(przepis_klopsiki);
        przepisy.add(przepis_herbata);
        przepisy.add(przepis_kawa);

//
//1. wybierz przepisy które mają mniej niż dwa składniki
        // tu pisz



//2. wybierz przepisy które mają wiecej niż 10 kroków.



//3. znajdź przepisy na porę roku LATO
//4. znajdź przepisy dłuższe (ilość kroków) niż 5 i które na liście składników zawierają "Jajko" w ilości 5
//5. zbierz kolekcję wszystkich unikalnych składników. Nie bierz pod uwagę ilości składnika, tylko jego nazwę podczas porównania
//6. Zbierz kolekcję wszystkich unikalnych kroków
//7. Zbierz kolekcję wszystkich treści przepisów (string'ów)
//8. Zbierz kolekcję wszystkich unikalnych nazw składników.
//9. Zbierz kolekcję składników z przepisów na zimę, z których przepisy mają mniej więcej 5 kroków.
    }

}
